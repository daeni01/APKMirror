package taco.apkmirror.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.MITLicense;
import de.psdev.licensesdialog.model.Notice;
import de.psdev.licensesdialog.model.Notices;
import taco.apkmirror.R;

public class PreferencesFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(final Bundle savedInstanceState, final String rootKey) {
        addPreferencesFromResource(R.xml.preferences);

        Preference sourceCode = findPreference("source_code");
        Preference libs = findPreference("libs");
        Preference xda = findPreference("xda");

        if (sourceCode != null) {
            sourceCode.setOnPreferenceClickListener(preference -> {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/TacoTheDank/APKMirror"));
                startActivity(i);
                return true;
            });
        }

        if (libs != null) {
            libs.setOnPreferenceClickListener(preference -> {
                final Notices n = new Notices();
                n.addNotice(new Notice("jsoup: Java HTML Parser", "https://github.com/jhy/jsoup", "Copyright (c) 2009-2020 Jonathan Hedley <https://jsoup.org/>", new MITLicense()));
                n.addNotice(new Notice("Android-AdvancedWebView (forked from delight-im/Android-AdvancedWebView)", "https://github.com/TacoTheDank/Android-AdvancedWebView", "Copyright (c) delight.im (https://www.delight.im/)", new MITLicense()));
                n.addNotice(new Notice("Material Dialogs", "https://github.com/afollestad/material-dialogs", "Copyright (c) 2018 Aidan Follestad", new MITLicense()));
                n.addNotice(new Notice("LicensesDialog", "https://github.com/PSDev/LicensesDialog", "Copyright 2013 Philip Schiffer", new ApacheSoftwareLicense20()));

                new LicensesDialog.Builder(requireActivity())
                        .setNotices(n)
                        .setTitle(getString(R.string.libraries))
                        .build()
                        .show();
                return true;
            });
        }

        if (xda != null) {
            xda.setOnPreferenceClickListener(preference -> {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://forum.xda-developers.com/android/apps-games/apkmirror-web-app-t3450564"));
                startActivity(i);
                return true;
            });
        }
    }
}

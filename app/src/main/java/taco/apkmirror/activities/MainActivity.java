package taco.apkmirror.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemReselectedListener;
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener;

import im.delight.android.webview.AdvancedWebView;
import taco.apkmirror.BuildConfig;
import taco.apkmirror.R;
import taco.apkmirror.classes.PageAsync;
import taco.apkmirror.databinding.ActivityMainBinding;
import taco.apkmirror.interfaces.AsyncResponse;
import taco.apkmirror.util.ClipboardUtil;

public class MainActivity extends AppCompatActivity implements AdvancedWebView.Listener, AsyncResponse {

    private static final String APKMIRROR_URL = "https://www.apkmirror.com/";
    private static final String APKMIRROR_UPLOAD_URL = "https://www.apkmirror.com/apk-upload/";
    private static final int[][] COLOR_STATES = {
            new int[]{android.R.attr.state_checked},
            new int[]{-android.R.attr.state_checked}
    };
    private final int navigationHome = R.id.navigation_home;
    private final int navigationUpload = R.id.navigation_upload;
    private final int navigationSettings = R.id.navigation_settings;
    private final int navigationExit = R.id.navigation_exit;
    private boolean saveUrl;
    private Integer shortAnimDuration;
    private Integer previsionThemeColor = Color.parseColor("#FF8B14");
    private SharedPreferences sharedPreferences;
    private NfcAdapter nfcAdapter;
    private ActivityMainBinding binding;

    /**
     * Listens for user clicking on the tab again.
     * We first check if the page is scrolled.
     * If so we move to top, otherwise we refresh the page.
     */
    private final OnNavigationItemReselectedListener tabReselectListener = menuItem -> {
        int itemId = menuItem.getItemId();
        loadUrlToWebview(itemId == navigationHome ? APKMIRROR_URL
                : itemId == navigationUpload ? APKMIRROR_UPLOAD_URL : null);
    };
    private final WebChromeClient chromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView v, int progress) {
            // update the progressbar value
            ObjectAnimator oa = ObjectAnimator.ofInt(binding.mainProgressBar, "progress", progress);
            oa.setDuration(100); // 0.5 second
            oa.setInterpolator(new DecelerateInterpolator());
            oa.start();
        }
    };
    private boolean settingsShortcut = false;
    private boolean triggerAction = true;
    private final OnNavigationItemSelectedListener tabSelectListener = new OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if (triggerAction) {
                switch (menuItem.getItemId()) {
                    case navigationHome:
                        //Home pressed
                        if (binding.settingsLayoutFragment.getVisibility() == View.VISIBLE) {
                            //settings is visible, gonna hide it
                            if (binding.mainWebview.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                                binding.mainWebview.loadUrl(APKMIRROR_URL);
                            }
                            crossFade(binding.settingsLayoutFragment, binding.webContainer);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                changeUIColor(previsionThemeColor);
                            }
                        } else {
                            //settings is not visible, load url
                            binding.mainWebview.loadUrl(APKMIRROR_URL);
                        }
                        break;
                    case navigationUpload:
                        //Upload pressed
                        if (binding.settingsLayoutFragment.getVisibility() == View.VISIBLE) {
                            //settings is visible, gonna hide it
                            if (!binding.mainWebview.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                                binding.mainWebview.loadUrl(APKMIRROR_UPLOAD_URL);
                            }
                            crossFade(binding.settingsLayoutFragment, binding.webContainer);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                changeUIColor(previsionThemeColor);
                            }
                        } else {
                            //settings is not visible, load url
                            binding.mainWebview.loadUrl(APKMIRROR_UPLOAD_URL);
                        }
                        break;
                    case navigationSettings:
                        //Settings pressed
                        if (binding.firstLoadingView.getVisibility() == View.VISIBLE) {
                            binding.firstLoadingView.setVisibility(View.GONE);
                        }
                        crossFade(binding.webContainer, binding.settingsLayoutFragment);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            changeUIColor(ContextCompat.getColor(MainActivity.this,
                                    R.color.colorPrimary));
                        }
                        break;
                    case navigationExit:
                        //Exit pressed
                        finish();
                        break;
                }
            }
            triggerAction = true;
            return true;
        }
    };

    private void loadUrlToWebview(@Nullable final String url) {
        if (url == null) {
            return;
        }
        final int webScrollY = binding.mainWebview.getScrollY();
        //Upload re-pressed
        if (webScrollY == 0) {
            //Load url
            binding.mainWebview.loadUrl(url);
        } else {
            //Scroll to top
            binding.mainWebview.setScrollY(0);
        }
    }

    private void initNavigation() {
        //Making the bottom navigation do something
        binding.navigation.setOnNavigationItemSelectedListener(tabSelectListener);
        binding.navigation.setOnNavigationItemReselectedListener(tabReselectListener);

        if (sharedPreferences.getBoolean("show_exit", false)) {
            binding.navigation.inflateMenu(R.menu.navigation_exit);
            binding.navigation.invalidate();
        }
    }

    private void initSearchFab() {
        boolean fab = sharedPreferences.getBoolean("fab", true);
        if (fab) {
            binding.fabSearch.show();
            binding.fabSearch.setOnClickListener(v -> search());
        }
    }

    private void initWebView(String url) {
        binding.mainWebview.setListener(this, this);
        binding.mainWebview.addPermittedHostname("apkmirror.com");
        binding.mainWebview.setWebChromeClient(chromeClient);
        binding.mainWebview.setUploadableFileTypes("application/vnd.android.package-archive");
        binding.mainWebview.loadUrl(url);
        binding.refreshLayout.setOnRefreshListener(() -> binding.mainWebview.reload());
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.mainWebview.onResume();
    }

    @Override
    protected void onPause() {
        binding.mainWebview.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        binding.mainWebview.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setTheme(R.style.AppTheme);
            binding = ActivityMainBinding.inflate(getLayoutInflater());
            setContentView(binding.getRoot());

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            initSearchFab();
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            shortAnimDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

            initNavigation();
            saveUrl = sharedPreferences.getBoolean("save_url", false);
            String url;

            Intent link = getIntent();
            Uri data = link.getData();

            if (data != null) {
                //app was opened from browser
                url = data.toString();
            } else {
                //data is null which means it was either launched from shortcuts or normally
                Bundle bundle = link.getExtras();
                if (bundle == null) {
                    //Normal start from launcher
                    if (saveUrl) {
                        url = sharedPreferences.getString("last_url", APKMIRROR_URL);
                    } else {
                        url = APKMIRROR_URL;
                    }
                } else {
                    //Ok it was shortcuts, check if it was settings
                    String bundleUrl = bundle.getString("url");
                    if (bundleUrl != null) {
                        if (bundleUrl.equals("apkmirror://settings")) {
                            //It was settings
                            url = APKMIRROR_URL;
                            binding.navigation.setSelectedItemId(navigationSettings);
                            crossFade(binding.webContainer, binding.settingsLayoutFragment);
                            settingsShortcut = true;
                        } else {
                            url = bundleUrl;
                        }
                    } else {
                        if (saveUrl) {
                            url = sharedPreferences.getString("last_url", APKMIRROR_URL);
                        } else {
                            url = APKMIRROR_URL;
                        }
                    }
                }
            }

            initWebView(url);

            // I know not the best solution xD
            if (!settingsShortcut) {
                binding.firstLoadingView.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    if (binding.firstLoadingView.getVisibility() == View.VISIBLE) {
                        crossFade(binding.firstLoadingView, binding.webContainer);
                    }
                }, 2000);
            }

        } catch (final RuntimeException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.runtime_error_dialog_content)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> finish())
                    .setNeutralButton(R.string.copy_log, (dialog, which) ->
                            ClipboardUtil.copyToClipboard(
                                    MainActivity.this, "log", e.toString()))
                    .show();
        }
    }

    @Override
    protected void onStop() {
        if (saveUrl && !binding.mainWebview.getUrl().equals("apkmirror://settings")) {
            sharedPreferences.edit().putString("last_url", binding.mainWebview.getUrl()).apply();
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        binding.mainWebview.onActivityResult(requestCode, resultCode, i);
    }

    @Override
    public void onBackPressed() {
        if (binding.settingsLayoutFragment.getVisibility() == View.VISIBLE) {
            crossFade(binding.settingsLayoutFragment, binding.webContainer);
            if (binding.mainWebview.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                triggerAction = false;
                binding.navigation.setSelectedItemId(navigationUpload);
            } else {
                triggerAction = false;
                binding.navigation.setSelectedItemId(navigationHome);
            }
            return;
        }
        if (!binding.mainWebview.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // Next line causes crash
        //webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Next line causes crash
        //webView.restoreState(savedInstanceState);
    }

    private void runAsync(String url) {
        //getting apps
        PageAsync pageAsync = new PageAsync();
        pageAsync.response = MainActivity.this;
        pageAsync.execute(url);
    }

    private void search() {
        new MaterialDialog.Builder(this)
                .title(R.string.search)
                .inputRange(1, 100)
                .input(R.string.search, R.string.nothing, (dialog, input) -> {
                })
                .onPositive((dialog, which) -> {
                    if (dialog.getInputEditText() != null) {
                        binding.mainWebview.loadUrl("https://www.apkmirror.com/?s=" + dialog.getInputEditText().getText());
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.search_error),
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .negativeText(android.R.string.cancel)
                .show();
    }

    private void crossFade(final View toHide, View toShow) {
        toShow.setAlpha(0f);
        toShow.setVisibility(View.VISIBLE);

        toShow.animate()
                .alpha(1f)
                .setDuration(shortAnimDuration)
                .setListener(null);

        toHide.animate()
                .alpha(0f)
                .setDuration(shortAnimDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator a) {
                        toHide.setVisibility(View.GONE);
                    }
                });
    }

    private void download(String url, String name) {
        if (sharedPreferences.getBoolean("external_download", false)) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } else {
            if (AdvancedWebView.handleDownload(this, url, name)) {
                Toast.makeText(MainActivity.this, getString(R.string.download_started),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, getString(R.string.cant_download),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isWritePermissionGranted() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onProcessFinish(Integer themeColor) {
        // updating interface
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            changeUIColor(themeColor);
        }
        previsionThemeColor = themeColor;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeUIColor(Integer color) {
        ValueAnimator anim = ValueAnimator.ofArgb(previsionThemeColor, color);
        anim.setEvaluator(new ArgbEvaluator());

        anim.addUpdateListener(vA -> {
            int getAV = (Integer) vA.getAnimatedValue();
            binding.mainProgressBar.getProgressDrawable().setColorFilter(
                    new LightingColorFilter(0xFF000000, getAV));
            setSystemBarColor(getAV);
            final ColorStateList colorStateList = new ColorStateList(COLOR_STATES,
                    new int[]{(int) vA.getAnimatedValue(), R.color.inactive});
            binding.navigation.setItemTextColor(colorStateList);
            binding.navigation.setItemIconTintList(colorStateList);
            binding.fabSearch.setBackgroundTintList(ColorStateList.valueOf(getAV));
        });

        anim.setDuration(shortAnimDuration);
        anim.start();
        binding.refreshLayout.setColorSchemeColors(color, color, color);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setSystemBarColor(int color) {
        int clr;
        //this makes the color darker or use a nicer orange color
        // Note: Do NOT replace this with previsionThemeColor
        if (color == Color.parseColor("#FF8B14")) {
            clr = Color.parseColor("#F47D20");
        } else {
            float[] hsv = new float[3];
            Color.colorToHSV(color, hsv);
            hsv[2] *= 0.8f;
            clr = Color.HSVToColor(hsv);
        }
        Window w = MainActivity.this.getWindow();
        w.setStatusBarColor(clr);
        if (sharedPreferences.getBoolean("color_navigation_bar", false)) {
            w.setNavigationBarColor(clr);
        }
    }

    private void setupNFC(String url) {
        if (nfcAdapter != null) { // in case there is no NFC
            try {
                // create an NDEF message containing the current URL:
                NdefRecord rec = NdefRecord.createUri(url); // url: current URL (String or Uri)
                NdefMessage ndef = new NdefMessage(rec);
                // make it available via Android Beam:
                nfcAdapter.setNdefPushMessage(ndef, this, this);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPageFinished(String url) {
        binding.mainProgressBarContainer.animate()
                .alpha(0f)
                .setDuration(getResources().getInteger(android.R.integer.config_longAnimTime))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.mainProgressBarContainer.setVisibility(View.GONE);
                    }
                });
        if (binding.refreshLayout.isRefreshing()) {
            binding.refreshLayout.setRefreshing(false);
        }
    }

    //WebView factory methods below
    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        if (!url.contains("https://www.apkmirror.com/wp-content/")) {
            runAsync(url);
            setupNFC(url);

            //Updating bottom navigation
            switch (binding.navigation.getSelectedItemId()) {
                case navigationHome:
                    if (url.equals(APKMIRROR_UPLOAD_URL)) {
                        triggerAction = false;
                        binding.navigation.setSelectedItemId(navigationUpload);
                    }
                    break;
                case navigationUpload:
                    if (!url.equals(APKMIRROR_UPLOAD_URL)) {
                        triggerAction = false;
                        binding.navigation.setSelectedItemId(navigationHome);
                    }
                    break;
            }

            //Showing progress bar
            binding.mainProgressBarContainer.animate()
                    .alpha(1f)
                    .setDuration(shortAnimDuration)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator a) {
                            super.onAnimationStart(a);
                            binding.mainProgressBarContainer.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        if (errorCode == -2) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(getString(R.string.error_while_loading_page) + " " + failingUrl +
                            "(" + errorCode + " " + description + ")")
                    .setPositiveButton(R.string.refresh, (dialog, which) -> {
                        binding.mainWebview.reload();
                        dialog.dismiss();
                    })
                    .setNegativeButton(R.string.dismiss, (dialog, which) -> dialog.dismiss())
                    .show();
        }
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType,
                                    long contentLength, String contentDisposition, String userAgent) {
        if (isWritePermissionGranted()) {
            download(url, suggestedFilename);
        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.write_permission)
                    .setMessage(R.string.storage_access)
                    .setPositiveButton(R.string.request_permission, (dialog, which) -> {
                        //Request permission
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, 1);
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }
    }

    @Override
    public void onExternalPageRequest(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}

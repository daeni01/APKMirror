package taco.apkmirror.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import taco.apkmirror.R;

public class ClipboardUtil {

    /**
     * Copies the text to the clipboard.
     *
     * @param context The context to pass.
     * @param label   User-visible label for the clip data.
     * @param text    The actual text in the clip.
     */
    public static void copyToClipboard(final Context context, final CharSequence label,
                                       final CharSequence text) {
        // Gets a handle to the clipboard service.
        final ClipboardManager clipboard = ContextCompat.getSystemService(context, ClipboardManager.class);
        if (clipboard != null) {
            // Creates a new text clip to put on the clipboard.
            final ClipData data = ClipData.newPlainText(label, text);
            clipboard.setPrimaryClip(data);
        } else {
            Toast.makeText(context, context.getString(R.string.clip_error), Toast.LENGTH_LONG).show();
        }
    }
}

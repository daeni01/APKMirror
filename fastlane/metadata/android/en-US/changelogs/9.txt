1.3:
- App may now scroll a bit smoother for devices using API 26 (Oreo 8.0) and up (your mileage may vary)
- Splash screen is no longer black for a second
- Settings tab now looks a bit nicer
1.3.1:
- Improve bottom bar icons
- Fix F-Droid build
